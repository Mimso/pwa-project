import './App.css';
import Slides from './components/slides';
import './css/slides.css';
import { app, database } from './firebaseConfig';

function App() {
  return (
    <Slides database={database}/>
  );
}

export default App;